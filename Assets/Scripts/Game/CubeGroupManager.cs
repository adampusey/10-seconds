﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGroupManager : MonoBehaviour
{
    GameObject[] cubeArray;
    GameObject chosenCube;
    int index;
    bool restart = true;

    // Start is called before the first frame update
    void Start()
    {
        cubeArray = GameObject.FindGameObjectsWithTag("CubeLube");
    }

    IEnumerator Test()
    {
        restart = false;
        selectRandomCube();
        yield return new WaitForSeconds(4);
        restart = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(restart == true)
        {
            StartCoroutine(Test());
        }

        if (cubeArray.Length == 0)
        {
            GameController.
        }
    }

    void selectRandomCube()
    {
        cubeArray = GameObject.FindGameObjectsWithTag("CubeLube");
        index = Random.Range(0, cubeArray.Length);
        chosenCube = cubeArray[index];
        Debug.Log(chosenCube);
        chosenCube.GetComponent<Rigidbody>().useGravity = true;
        chosenCube.GetComponent<Rigidbody>().isKinematic = false;
    }
}

