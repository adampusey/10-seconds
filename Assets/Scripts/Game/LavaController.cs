﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaController : MonoBehaviour
{
    Material MaterialLava { get; set; }
    MeshCollider Collider { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        MaterialLava = gameObject.GetComponent<MeshRenderer>().materials[0];
        Collider = gameObject.GetComponent<MeshCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        MaterialLava.mainTextureOffset += new Vector2(1, 1) * (Time.deltaTime * 0.0075f);
    }
}
