﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // Components
    Camera Camera { get; set; }
    Rigidbody Rigidbody { get; set; }
    AudioSource MyAudioSource { get; set; }

    // Properties
    public Vector3 CameraOffset { get; set; } = new Vector3(0, 10, -10);
    public float MaxVelocity { get; set; } = 5f;
    bool Enabled { get; set; }
    bool HasRigidbody => Rigidbody != null;
    Quaternion PreviousCameraRotation { get; set; }

    bool MoveLeft { get; set; } = false;
    bool MoveRight { get; set; } = false;
    bool MoveForward { get; set; } = false;
    bool MoveBackward { get; set; } = false;
    bool Jump { get; set; } = false;

    // Start is called before the first frame update
    void Start()
    {
        Enabled = true;
        Rigidbody = gameObject.GetComponent<Rigidbody>();
        MyAudioSource = gameObject.GetComponent<AudioSource>();
        InitCamera();
    }

    public void Die()
    {
        Camera.cullingMask = 0;
        MyAudioSource.PlayOneShot(MyAudioSource.clip);
        Destroy(this, 3);
    }

    private void OnDestroy()
    {
        SceneManager.LoadSceneAsync(0);
    }

    void InitCamera()
    {
        Camera = gameObject.GetComponentInChildren(typeof(Camera)) as Camera;
        PreviousCameraRotation = Camera.transform.rotation;
    }

    void Update()
    {
        // Forward
        if (Input.GetKey(KeyCode.W))
        {
            MoveForward = true;
        }
        else
        {
            MoveForward = false;
        }

        // Left
        if (Input.GetKey(KeyCode.A))
        {
            MoveLeft = true;
        }
        else
        {
            MoveLeft = false;
        }

        // Right
        if (Input.GetKey(KeyCode.D))
        {
            MoveRight = true;
        }
        else
        {
            MoveRight = false;
        }

        // Backward
        if (Input.GetKey(KeyCode.S))
        {
            MoveBackward = true;
        }
        else
        {
            MoveBackward = false;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            Jump = true;
        }
        else
        {
            Jump = false;
        }

    }

    private void FixedUpdate()
    {
        if (gameObject.activeSelf && !GameController.Paused)
        {
            PhysicsUpdate();
        }
    }

    private void LateUpdate()
    {
        CameraUpdate();
    }

    void CameraUpdate()
    {
        Camera.transform.LookAt(gameObject.transform);
        Camera.transform.rotation = PreviousCameraRotation;
        Camera.transform.position = gameObject.transform.position + CameraOffset;
    }

    void PhysicsUpdate()
    {
        if (!HasRigidbody) return;

        // Apply force in direction relating to keypress

        // Forward
        if (MoveForward && !MoveBackward)
        {
            Rigidbody.AddForce(new Vector3(0, 0, 2) * MaxVelocity, ForceMode.Force);
        }

        // Left
        if (MoveLeft && !MoveRight)
        {
            Rigidbody.AddForce(new Vector3(-2, 0, 0) * MaxVelocity, ForceMode.Force);
        }

        // Right
        if (MoveRight && !MoveLeft)
        {
             Rigidbody.AddForce(new Vector3(2, 0, 0) * MaxVelocity, ForceMode.Force);
        }

        // Backward
        if (MoveBackward && !MoveForward)
        {
            Rigidbody.AddForce(new Vector3(0, 0, -2) * MaxVelocity, ForceMode.Force);
        }

        if (Jump && (int)Rigidbody.velocity.y == 0)
        {
            Rigidbody.AddForce(Vector3.up * 2);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Killzone")
        {
            Die();
        }
    }
}
